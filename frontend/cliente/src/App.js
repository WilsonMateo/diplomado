import "./App.css";
import SignIn from "./components/SignIn";
import SignUp from "./components/SignUp";
import Cards from "./admin/TeacherForm";
import TeacherForm from "./admin/TeacherForm";
import CourseForm from "./admin/CourseForm";
import List from "./admin/List";
import Inicio from "./admin/inicio";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "antd/dist/antd.css";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={SignIn} />
          <Route exact path="/SignUp" component={SignUp} />
          <Route exact path="/Cards" component={Cards} />
          <Route exact path="/TeacherForm" component={TeacherForm} />
          <Route exact path="/CourseForm" component={CourseForm} />
          <Route exact path="/List" component={List} />
          <Route exact path="/Inicio" component={Inicio} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
