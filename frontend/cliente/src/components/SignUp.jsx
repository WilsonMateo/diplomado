import React, { useState } from "react";
import "../Styles/SignIn.css";
import { Form, Input, Button, Col, Row } from "antd";
import { Content } from "antd/lib/layout/layout";
import registro from "../img/login.png";

const SignUp = () => {
  //State para iniciar sesion
  const [usuario, guardarUsuario] = useState({
    username: "",
    email: "",
    password: "",
    confirm: "",
  });

  //Extraer de Usuario
  const { username, email, password, confirm } = usuario;

  const onChange = (e) => {
    guardarUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };
  //Cuando el usuario quiere iniciar sesion
  const onSubmit = (e) => {
    e.preventDefault();

    //pasarloalaction
  };
  const [form] = Form.useForm();

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
  };

  return (
    <Content style={{ height: " 100vh" }} className="content-center">
      <Row>
        <Col span={12}>
          <img
            src={registro}
            alt="registro"
            style={{ width: " 200%", height: "100vh" }}
          />
        </Col>
        <Col span={12} className="center-SingOut" style={{ display: "flex" }}>
          <div className="formulario" style={{ marginRight: " 190px" }}>
            <Form
              onSubmit={onSubmit}
              style={{ width: " 150%" }}
              layout="vertical"
              form={form}
              name="register"
              onFinish={onFinish}
            >
              <h1 style={{ fontSize: "55px", color: "rgb(255 255 255)" }}>
                Sign up
              </h1>

              <Form.Item
                style={{ color: "rgb(255 255 255)" }}
                className="username"
                label="Username"
                name="username"
                rules={[
                  { required: true, message: "Please input your username!" },
                ]}
              >
                <Input
                  block
                  style={{ borderRadius: "32px", height: "39px" }}
                  type="text"
                  id="username"
                  name="username"
                  value={username}
                  onChange={onChange}
                />
              </Form.Item>

              <Form.Item
                className="email"
                name="email"
                label="E-mail"
                rules={[
                  {
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                  {
                    required: true,
                    message: "Please input your E-mail!",
                  },
                ]}
              >
                <Input
                  style={{ borderRadius: "32px", height: "39px" }}
                  type="text"
                  id="email"
                  name="email"
                  value={email}
                  onChange={onChange}
                />
              </Form.Item>

              <Form.Item
                className="password"
                name="password"
                label="Password"
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
                hasFeedback
              >
                <Input.Password
                  style={{ borderRadius: "32px", height: "39px" }}
                  type="password"
                  id="password"
                  name="password"
                  value={password}
                  onChange={onChange}
                />
              </Form.Item>

              <Form.Item
                className="confirm"
                name="confirm"
                label="Confirm Password"
                dependencies={["password"]}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Please confirm your password!",
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }

                      return Promise.reject(
                        "The two passwords that you entered do not match!"
                      );
                    },
                  }),
                ]}
              >
                <Input.Password
                  style={{ borderRadius: "32px", height: "39px" }}
                  type="password"
                  id="confirm"
                  name="confirm"
                  value={confirm}
                  onChange={onChange}
                />
              </Form.Item>

              <Button
                className="button2"
                type="primary"
                shape="round"
                htmlType="submit"
                block
                style={{
                  backgroundColor: "#3860F8",
                  outline: "none",
                  height: "39px",
                }}
              >
                Register
              </Button>
            </Form>
          </div>
        </Col>
      </Row>
    </Content>
  );
};

export default SignUp;
