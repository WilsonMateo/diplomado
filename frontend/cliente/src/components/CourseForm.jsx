import React from "react";
import { Form, Input, Button } from "antd";
import { Content } from "antd/lib/layout/layout";
import "../Styles/CourseForm.css";

const CourseForm = () => {
  return (
    <Content style={{ height: " 100vh" }} className="content-center">
      <div className="CourseForm" style={{ background: "#343F73" }}>
        <Form
          style={{ width: "140%", background: "#343F73" }}
          layout="vertical"
          name="CourseForm"
          initialValues={{
            remember: true,
          }}
        >
          <Form.Item
            className="coursename"
            label="Nombre del Curso"
            name="coursename"
            rules={[
              {
                required: true,
                message: "Por favor ingrese un curso!",
              },
            ]}
          >
            <Input style={{ borderRadius: "8.61px", color: "#495057" }} />
          </Form.Item>

          <Form.Item
            className="Course"
            label="Descripción del Curso"
            name="Course"
            rules={[
              {
                required: true,
                message: "Por favor ingrese su descripción!",
              },
            ]}
          >
            <Input style={{ borderRadius: "8.61px", color: "#495057" }} />
          </Form.Item>

          <Form.Item
            className="schedule"
            label="Horario del curso"
            name="schedule"
            rules={[
              {
                required: true,
                message: "Por favor ingrese un horario!",
              },
            ]}
          >
            <Input style={{ borderRadius: "8.61px", color: "#495057" }} />
          </Form.Item>

          <Form.Item
            className="professor"
            label="Catedrático"
            name="professor"
            rules={[
              {
                required: true,
                message: "Por favor ingrese un Catedrático!",
              },
            ]}
          >
            <Input style={{ borderRadius: "8.61px", color: "#495057" }} />
          </Form.Item>

          <Form.Item className="buttton">
            <Button
              type="primary"
              shape="round"
              htmlType="submit"
              style={{
                backgroundColor: "#6B63FF",
                bottom: "auto",
                height: "39px",
                width: "150px",
              }}
            >
              Crear Usuario
            </Button>
          </Form.Item>
        </Form>
      </div>
    </Content>
  );
};

export default CourseForm;
