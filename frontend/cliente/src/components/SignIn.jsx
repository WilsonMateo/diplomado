import React, { useState } from "react";
import { Form, Input, Button, Checkbox, Col, Row } from "antd";
import { Content } from "antd/lib/layout/layout";
import login from "../img/login.png";
import "../Styles/SignIn.css";

const SignIn = () => {
  //State para iniciar sesion
  const [usuario, guardarUsuario] = useState({
    username: "",
    password: "",
  });

  //Extraer de Usuario
  const { username, password } = usuario;

  const onChange = (e) => {
    guardarUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };

  //Cuando el usuario quiere iniciar sesion
  const onSubmit = (e) => {
    e.preventDefault();

    //pasarloalaction
  };

  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };

  return (
    <Content style={{ height: " 100vh" }} className="content-center">
      <Row>
        <Col span={12}>
          <img
            src={login}
            alt="login"
            style={{ width: " 200%", height: "100vh" }}
          />
        </Col>
        <Col span={12} className="center-SingOut" style={{ display: "flex" }}>
          <div className="formulario" style={{ marginRight: " 190px" }}>
            <Form
              onSubmit={onSubmit}
              style={{ width: " 150%" }}
              layout="vertical"
              name="SignIn"
              initialValues={{
                remember: true,
              }}
            >
              <h1
                className="text-style"
                style={{ fontSize: "55px", color: "rgb(255 255 255)" }}
              >
                Sign in
              </h1>
              <Form.Item
                className="user"
                label="Username"
                name="username"
                rules={[
                  {
                    required: true,
                    message: "Please input your username!",
                  },
                ]}
              >
                <Input
                  style={{ borderRadius: "32px", height: "39px" }}
                  type="text"
                  id="username"
                  name="username"
                  value={username}
                  onChange={onChange}
                />
              </Form.Item>

              <Form.Item
                className="password"
                label="Password"
                name="password"
                value={password}
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
              >
                <Input.Password
                  style={{ borderRadius: "32px", height: "39px" }}
                  type="password"
                  id="password"
                  name="password"
                  value={password}
                  onChange={onChange}
                />
              </Form.Item>

              <Form.Item
                className="checkbox"
                {...tailLayout}
                name="remember"
                valuePropName="checked"
              >
                <Checkbox>Remember me</Checkbox>
              </Form.Item>

              <Form.Item className="button">
                <Button
                  type="primary"
                  shape="round"
                  htmlType="submit"
                  block
                  style={{
                    backgroundColor: "#3860F8",
                    outline: "none",
                    height: "39px",
                  }}
                >
                  Log in
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Col>
      </Row>
    </Content>
  );
};

export default SignIn;
