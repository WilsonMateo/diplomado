import React from "react";
import { Form, Input, Button } from "antd";
import { Content } from "antd/lib/layout/layout";
import "../Styles/TeacherForm.css";

const TeacherForm = () => {
  return (
    <Content style={{ height: " 100vh" }} className="content-center">
      <div className="Container" style={{ background: "#343F73" }}>
        <Form
          style={{ width: "140%" }}
          layout="vertical"
          name="TeacherForm"
          initialValues={{
            remember: true,
          }}
        >
          <Form.Item
            className="teacherform"
            label="Nombre del Profesor"
            name="TeacherForm"
            rules={[
              {
                required: true,
                message: "Por favor ingrese un nombre!",
              },
            ]}
          >
            <Input style={{ borderRadius: "8.61px", color: "#495057" }} />
          </Form.Item>

          <Form.Item
            className="lastname"
            label="Apellidos"
            name="Lastname"
            rules={[
              {
                required: true,
                message: "Por favor ingrese su apellido!",
              },
            ]}
          >
            <Input style={{ borderRadius: "8.61px", color: "#495057" }} />
          </Form.Item>

          <Form.Item
            className="email"
            label="Correo Electronico"
            name="Email"
            rules={[
              {
                required: true,
                message: "Por favor ingrese su correo!",
              },
            ]}
          >
            <Input style={{ borderRadius: "8.61px", color: "#495057" }} />
          </Form.Item>

          <Form.Item
            className="password"
            label="Contraseña"
            name="password"
            rules={[
              {
                required: true,
                message: "Por favor ingrese su contraseña!",
              },
            ]}
          >
            <Input.Password
              style={{ borderRadius: "8.61px", color: "#495057" }}
            />
          </Form.Item>

          <Form.Item className="buttton">
            <Button
              type="primary"
              shape="round"
              htmlType="submit"
              style={{
                backgroundColor: "#E17551",
                bottom: "auto",
                height: "39px",
                width: "150px",
              }}
            >
              Crear Usuario
            </Button>
          </Form.Item>
        </Form>
      </div>
    </Content>
  );
};

export default TeacherForm;
