import React, { Component } from 'react';
import { Table, Input, Button, Space } from 'antd';
import Highlighter from 'react-highlight-words';
import { SearchOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import Container from 'react-bootstrap/Container'
import ListTeacher from './listTeacher'




const data = [
    {
        key: '1',
        Nombre: 'Frida ',
        Apellido: "Larios",
        Correo: 'fridalarios862@gmail.com',
        Edit: <EditOutlined style={{ fontSize: '16px', color: '#08c', cursor: 'pointer' }} />,
        Delete: <DeleteOutlined style={{ fontSize: '16px', color: '#ff0000', cursor: 'pointer' }} />,

    },
    {
        key: '2',
        Nombre: 'Wilson ',
        Apellido: "Yax",
        Correo: 'wilbalyax@gmail.com',
        Edit: <EditOutlined style={{ fontSize: '16px', color: '#08c', cursor: 'pointer' }} />,
        Delete: <DeleteOutlined style={{ fontSize: '16px', color: '#ff0000', cursor: 'pointer' }} />,

    },
    {
        key: '3',
        Nombre: 'Estuardo ',
        Apellido: "Larios",
        Correo: 'estuardolariosv@gmail.com',
        Edit: <EditOutlined style={{ fontSize: '16px', color: '#08c', cursor: 'pointer' }} />,
        Delete: <DeleteOutlined style={{ fontSize: '16px', color: '#ff0000', cursor: 'pointer' }} />,
    },
    {
        key: '4',
        Nombre: 'Edgar ',
        Apellido: "Balan",
        Correo: 'orlandobbernardino@gmail.com',
        Edit: <EditOutlined style={{ fontSize: '16px', color: '#08c', cursor: 'pointer' }} />,
        Delete: <DeleteOutlined style={{ fontSize: '16px', color: '#ff0000', cursor: 'pointer' }} />,
    },
];

class List extends Component {
    state = {
        searchText: '',
        searchedColumn: '',
    };

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
              </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 80 }}>
                        Reset
              </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                    text
                ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };
    render() {
        const columns = [
            {
                title: 'key',
                dataIndex: 'key',
                key: 'key',
                width: '30%',
                ...this.getColumnSearchProps('key'),
            },
            {
                title: 'Nombre',
                dataIndex: 'Nombre',
                key: 'Nombre',
                width: '30%',
                ...this.getColumnSearchProps('Nombre'),
            },
            {
                title: 'Apellido',
                dataIndex: 'Apellido',
                key: 'Apellido',
                width: '20%',
                ...this.getColumnSearchProps('Apellido'),
            },
            {
                title: 'Correo',
                dataIndex: 'Correo',
                key: 'Correo',
                ...this.getColumnSearchProps('Correo'),

            },
            {
                title: 'Edit',
                dataIndex: 'Edit',
                key: 'Edit',
            },
            {
                title: 'Delete',
                dataIndex: 'Delete',
                key: 'Delete',
            },
        ];
        return (
            <>
                <Container>
                    <h2 className="text-list">Lista de Alumnos</h2>
                    <Table columns={columns} dataSource={data} />;
                    <ListTeacher />
                </Container>

            </>
        )


    }
}

export default List;