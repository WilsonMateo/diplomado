import React from "react";
import { Card, Button } from 'antd';
import "../Styles/Cards.css";
import administracion from "../img/administracion.jpg";
import derecho from '../img/Derecho.jpg';
import civil from '../img/ingcivil.jpg';
import comunicacion from '../img/Comunicacion.jpg';
import mecatronica from '../img/Mecatronitaca.jpg';
import sistema from '../img/ing.jpg'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';




const { Meta } = Card;

const Cards = () => {

    return (
        <>
            <Row>
                <Col sm>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <Card className="flip-card-front"
                                style={{ width: 300 }}
                                cover={
                                    <img
                                        alt="administracion"
                                        src={administracion}
                                        title="Administracion"
                                    />
                                }
                            >
                                <Meta title="Administracion de Empresas" />
                            </Card>
                            <Card className="flip-card-back">

                                <Meta
                                    title="Administracion de Empresas"
                                    description="La Administración de Empresas es la disciplina encargada de la planificación, organización, dirección y control de los recursos de una organización, con el fin de obtener su máximo rendimiento. Estos recursos pueden ser humanos, financieros, materiales o tecnológicos."
                                />

                                <Button className="button"
                                    type="primary" shape="round" htmlType="submit" block
                                    style={{ backgroundColor: "#CFAD2F", outline: "none", height: "39px" }}>
                                    Inscribirse
                        </Button>

                            </Card>
                        </div>
                    </div>
                </Col>
                <Col sm>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <Card className="flip-card-front"
                                style={{ width: 300 }}
                                cover={
                                    <img
                                        alt="derecho"
                                        src={derecho}
                                        title="Derecho"
                                    />
                                }
                            >
                                <Meta title="Derecho" />
                            </Card>
                            <Card className="flip-card-back">

                                <Meta
                                    title="Derecho"
                                    description="La Administración de Empresas es la disciplina encargada de la planificación, organización, dirección y control de los recursos de una organización, con el fin de obtener su máximo rendimiento. Estos recursos pueden ser humanos, financieros, materiales o tecnológicos."
                                />

                                <Button className="button"
                                    type="primary" shape="round" htmlType="submit" block
                                    style={{ backgroundColor: "#CFAD2F", outline: "none", height: "39px" }}>
                                    Inscribirse
                        </Button>

                            </Card>
                        </div>
                    </div>
                </Col>
                <Col sm>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <Card className="flip-card-front"
                                style={{ width: 300 }}
                                cover={
                                    <img
                                        alt="civil"
                                        src={civil}
                                        title="Civil"
                                    />
                                }
                            >
                                <Meta title="Ingeniería Civil" />
                            </Card>
                            <Card className="flip-card-back">

                                <Meta
                                    title="Ingeniería Civil"
                                    description="La Administración de Empresas es la disciplina encargada de la planificación, organización, dirección y control de los recursos de una organización, con el fin de obtener su máximo rendimiento. Estos recursos pueden ser humanos, financieros, materiales o tecnológicos."
                                />

                                <Button className="button"
                                    type="primary" shape="round" htmlType="submit" block
                                    style={{ backgroundColor: "#CFAD2F", outline: "none", height: "39px" }}>
                                    Inscribirse
                        </Button>

                            </Card>
                        </div>
                    </div>
                </Col>
            </Row>
            {/*  */}
            <Row style={{ marginTop: "100px" }}>
                <Col sm>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <Card className="flip-card-front"
                                style={{ width: 300 }}
                                cover={
                                    <img
                                        alt="sistema"
                                        src={sistema}
                                        title="Ingeniería en Sistemas"
                                    />
                                }
                            >
                                <Meta title="Ingeniería en Sistemas" />
                            </Card>
                            <Card className="flip-card-back">

                                <Meta
                                    title="Ingeniería en Sistemas"
                                    description="La Administración de Empresas es la disciplina encargada de la planificación, organización, dirección y control de los recursos de una organización, con el fin de obtener su máximo rendimiento. Estos recursos pueden ser humanos, financieros, materiales o tecnológicos."
                                />

                                <Button className="button"
                                    type="primary" shape="round" htmlType="submit" block
                                    style={{ backgroundColor: "#CFAD2F", outline: "none", height: "39px" }}>
                                    Inscribirse
                        </Button>

                            </Card>
                        </div>
                    </div>
                </Col>
                <Col sm>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <Card className="flip-card-front"
                                style={{ width: 300 }}
                                cover={
                                    <img
                                        alt="mecatronica"
                                        src={mecatronica}
                                        title="Ingeniería en Mecatronica"
                                    />
                                }
                            >
                                <Meta title="Ingeniería en Mecatronica" />
                            </Card>
                            <Card className="flip-card-back">

                                <Meta
                                    title="Mecatronica"
                                    description="La Administración de Empresas es la disciplina encargada de la planificación, organización, dirección y control de los recursos de una organización, con el fin de obtener su máximo rendimiento. Estos recursos pueden ser humanos, financieros, materiales o tecnológicos."
                                />

                                <Button className="button"
                                    type="primary" shape="round" htmlType="submit" block
                                    style={{ backgroundColor: "#CFAD2F", outline: "none", height: "39px" }}>
                                    Inscribirse
                        </Button>

                            </Card>
                        </div>
                    </div>
                </Col>
                <Col sm>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <Card className="flip-card-front"
                                style={{ width: 300 }}
                                cover={
                                    <img
                                        alt="comunicación"
                                        src={comunicacion}
                                        title="Ciencias de la Comunicación"
                                    />
                                }
                            >
                                <Meta title="Ciencias de la Comunicación" />
                            </Card>
                            <Card className="flip-card-back">

                                <Meta
                                    title="Ciencias de la Comunicación"
                                    description="La Administración de Empresas es la disciplina encargada de la planificación, organización, dirección y control de los recursos de una organización, con el fin de obtener su máximo rendimiento. Estos recursos pueden ser humanos, financieros, materiales o tecnológicos."
                                />

                                <Button className="button"
                                    type="primary" shape="round" htmlType="submit" block
                                    style={{ backgroundColor: "#CFAD2F", outline: "none", height: "39px" }}>
                                    Inscribirse
                        </Button>

                            </Card>
                        </div>
                    </div>
                </Col>
            </Row>

        </>
    );
};

export default Cards;
//style={{ position: "fixed" }}