import React, { Component } from "react";
import Cards from "../components/Cards";
import { Layout, Menu, Breadcrumb } from "antd";
import {
  FileOutlined,
  HomeOutlined,
  FileAddOutlined,
  UserAddOutlined,
  UnorderedListOutlined,
  SafetyCertificateOutlined,
} from "@ant-design/icons";
const { Header, Content, Footer, Sider } = Layout;

export default class inicio extends Component {
  state = {
    collapsed: false,
  };

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

  render() {
    const { collapsed } = this.state;
    return (
      <div>
        <Layout style={{ minHeight: "100vh" }}>
          <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
            <div className="logo" />
            <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
              <Menu.Item key="Inicio" icon={<HomeOutlined />}>
                Inicio
              </Menu.Item>
              <Menu.Item key="Crear Curso" icon={<FileAddOutlined />}>
                Crear Curso
              </Menu.Item>

              <Menu.Item key="Agregar Profesor" icon={<UserAddOutlined />}>
                Agregar Profesor
              </Menu.Item>

              <Menu.Item
                key="Lista Profesores"
                icon={<UnorderedListOutlined />}
              >
                Lista Profesores
              </Menu.Item>

              <Menu.Item key="Lista Alumnos" icon={<UnorderedListOutlined />}>
                Lista Alumnos
              </Menu.Item>

              <Menu.Item key="Asistencia" icon={<SafetyCertificateOutlined />}>
                Asistencia
              </Menu.Item>

              <Menu.Item key=" Mis Cursos" icon={<FileOutlined />}>
                Mis Cursos
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Header className="site-layout-background" style={{ padding: 0 }} />
            <Content style={{ margin: "0 16px" }}>
              <Breadcrumb style={{ margin: "16px 0" }}>
                <p>Perfil</p>
              </Breadcrumb>
              <div
                className="site-layout-background"
                style={{ padding: 24, minHeight: 360 }}
              >
                <Cards />
              </div>
            </Content>
            <Footer style={{ textAlign: "center" }}>
              Ant Design ©2018 Created by Ant UED
            </Footer>
          </Layout>
        </Layout>
      </div>
    );
  }
}
